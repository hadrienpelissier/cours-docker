---
title: Conteneurs Docker
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
<!--
.bottom-bar[
  {{title}}
]
-->

---

# Conteneurs Docker
## *Modularisez et maîtrisez vos applications*

---

class: impact

# Docker en production

---

# Bonnes pratiques et outils

## Sécurité
- les user namespaces
  https://medium.com/@mccode/processes-in-containers-should-not-run-as-root-2feae3f0df3b

```bash
vim /etc/docker/daemon.json
adduser docker-userns -s /bin/false
service docker restart
cat /etc/subuid
cat /etc/passwd
docker run -d -it alpine sh
docker ps
htop
```

---

# Bonnes pratiques et outils

## Sécurité

- le benchmark Docker
  - https://github.com/docker/docker-bench-security/
  - Le livre *Mastering Docker*, de Russ McKendrick et Scott Gallagher

- Clair : l'analyse statique d'images Docker
---

# Gérer les logs des conteneurs

Avec Elasticsearch, Logstash et Kibana

---

# Monitorer des conteneurs

Avec Portainer

---

# Tests sur des conteneurs

Ansible comme source de healthcheck

---

<!-- # Exemples de cas pratiques :

Présentation d'un workflow Docker, du développement à la production -->
